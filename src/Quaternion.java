import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion {

   private double a;
   private double b;
   private double c;
   private double d;
   public static final double treshold = 0.000001;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
	  this.a = a;
	  this.b = b;
	  this.c = c;
	  this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.a;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return this.b;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return this.c;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return this.d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      StringBuffer sb = new StringBuffer();
      double[] da = {a, b, c, d};
      String[] sa = {"", "i", "j", "k"};
      for (int i = 0; i < da.length; i++) {
    	  if (i > 0 && da[i] >= 0) {
    		  sb.append("+");
    	  }
    	  if (da[i] % 1 == 0) {
    		  sb.append((int)da[i] + sa[i]);
    	  } else {
    		  sb.append(da[i] + sa[i]);
    	  }
      }
	  return sb.toString();
   }

   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) throws IllegalArgumentException {
	   double a = 0, b = 0, c = 0, d = 0;
	   String[] tokens = s.trim().split("(?=[+-])");

	   if (tokens.length != 4) {
		   throw new IllegalArgumentException("Expression \"" + s + "\" has wrong number of operands."
				   + " Must be 4, but is " + tokens.length + ".");
	   }
	   for (int i = 0; i < tokens.length; i++) {
		   String token = tokens[i].trim();
		   try {
			   if (i == 0) {
				   a = Double.parseDouble(token); 
			   } else {
				   char lastChar = token.charAt(token.length() - 1);
				   double dToken = Double.parseDouble(token.substring(0, token.length() - 1));
				
				   if ( (i == 1) && (lastChar == 'i') ) {
					   b = dToken;
				   } else if ( (i == 2) && (lastChar == 'j') ) {
					   c = dToken;
				   } else if ( (i == 3) && (lastChar == 'k') ) {
					   d = dToken;
				   } else {
					   throw new IllegalArgumentException("\"" + s + "\" has illegal or misplaced operand " + token + ".");
				   }
			   }
		   } catch (NumberFormatException nfe) {
			   throw new IllegalArgumentException("\"" + s + "\" has illegal operand " + token + ".");
		   }
	   }
	   return new Quaternion(a, b, c, d);
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(this.a, this.b, this.c, this.d);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return Math.abs(this.a) < treshold && Math.abs(this.b) < treshold &&
    		  Math.abs(this.c) < treshold && Math.abs(this.d) < treshold;
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(this.a, -this.b, -this.c, -this.d); 
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-this.a, -this.b, -this.c, -this.d);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(this.a + q.getRpart(), this.b + q.getIpart(),
    		  				this.c + q.getJpart(), this.d + q.getKpart());
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double resa = this.a * q.getRpart() - this.b * q.getIpart() - 
    		  		this.c * q.getJpart() - this.d * q.getKpart();
      double resb = this.a * q.getIpart() + this.b * q.getRpart() +
    		  		this.c * q.getKpart() - this.d * q.getJpart();
      double resc = this.a * q.getJpart() - this.b * q.getKpart() +
    		  		this.c * q.getRpart() + this.d * q.getIpart();
      double resd = this.a * q.getKpart() + this.b * q.getJpart() -
    		  		this.c * q.getIpart() + this.d * q.getRpart();
	  return new Quaternion(resa, resb, resc, resd);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(this.a * r, this.b * r, this.c * r, this.d * r);
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
	  if (isZero()) {
		  throw new IllegalArgumentException("Can't inverse if quaternion is zero.");
	  }
      double resa = a/(a*a+b*b+c*c+d*d);
      double resb = (-b)/(a*a+b*b+c*c+d*d);
      double resc = (-c)/(a*a+b*b+c*c+d*d);
      double resd = (-d)/(a*a+b*b+c*c+d*d);
	  return new Quaternion(resa, resb, resc, resd);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      return this.plus(q.opposite());
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()) {
    	  throw new IllegalArgumentException("Can't divide by zero quanternion.");
      }
	  return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
	  if (q.isZero()) {
	   	  throw new IllegalArgumentException("Can't divide by zero quanternion.");
	  }
	  return q.inverse().times(this);
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if ( (qo != null) && (qo instanceof Quaternion) ) {
    	  Quaternion q = (Quaternion) qo;    	  
          if (Math.abs(this.a - q.a) < treshold &&
              Math.abs(this.b - q.b) < treshold &&
              Math.abs(this.c - q.c) < treshold &&
              Math.abs(this.d - q.d) < treshold) {
             return true;
          }
      }
      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      return this.times(q.conjugate()).plus(q.times(this.conjugate())).times(1/2.);
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
	  long bits = Double.doubleToLongBits(this.a * this.b * this.c * this.d);
      return (int)((int)bits ^ (bits >> 32));
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(this.a * this.a + this.b * this.b + 
    		  		   this.c * this.c + this.d * this.d);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
      
      try { 
    	  Quaternion.valueOf("1+2i");
      } catch (IllegalArgumentException iae) {
    	  System.err.println(iae);
      }
      try {
    	  Quaternion.valueOf("1+2i+3j+4k+5l");
      } catch (IllegalArgumentException iae) {
    	  System.err.println(iae);
      }
      try {
    	  Quaternion.valueOf("1+2k+3j+4k");
      } catch (IllegalArgumentException iae) {
    	  System.err.println(iae);
      }
      try {
    	  Quaternion.valueOf("1+2ia+3j+4k");
      } catch (IllegalArgumentException iae) {
    	  System.err.println(iae);
      }
      try {
    	  Quaternion.valueOf("1+ai+3j+4k");
      } catch (IllegalArgumentException iae) {
    	  System.err.println(iae);
      }
      try {
    	  Quaternion.valueOf("1+2i+3j+4k").divideByLeft(new Quaternion(0, 0, 0, 0));
      } catch (IllegalArgumentException iae) {
    	  System.err.println(iae);
      }
   }
}
// end of file
